<?php

namespace App\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */


    public function index(Connection $conn)
    {


        // $queryBuilder = $conn->createQueryBuilder();
        // $data = $queryBuilder->select('fist_name')->from('employee')->execute()->fetch();
        $statement = $conn->prepare('SELECT * FROM employee');
        $statement->execute();
        $user = $statement->fetchAll();

        
        return $this->render('home/index.html.twig', [
            'firstName' => $user[0]['first_name'],
            'users' => $user,
        ]);

    }


}